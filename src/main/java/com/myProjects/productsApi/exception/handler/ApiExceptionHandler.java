package com.myProjects.productsApi.exception.handler;

import java.util.Date;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.myProjects.productsApi.exception.ProductNotFound;
import com.myProjects.productsApi.message.ErrorMessage;


//@RestControllerAdvice specialization of @component so that it is auto-detected via classpath scanning 
@RestControllerAdvice
public class ApiExceptionHandler {
	
	//capture exception and translate them to HTTP responses
	//handle several exceptions in the same method @ExceptionHandler(value = {ProductNotFound.class,exception1.class ...}) 
	
	@ExceptionHandler(value = {ProductNotFound.class}) 
	public ResponseEntity<ErrorMessage> productNotFound(ProductNotFound ex , WebRequest request){
		ErrorMessage errorMessage = new ErrorMessage(
											ex.getMessage(),
											new Date(),
											HttpStatus.NOT_FOUND.value(),
											request.getDescription(false)
										);			
		return new ResponseEntity<>(errorMessage,HttpStatus.NOT_FOUND);  
	} 
	
	
} 
  